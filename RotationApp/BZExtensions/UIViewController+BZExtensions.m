//
//  UIViewController+BZExtensions.m
//  RotationApp
//
//  Created by Boris on 4/15/16.
//  Copyright © 2016 Boris. All rights reserved.
//

#import "UIViewController+BZExtensions.h"

#import "BZExtensionsManager.h"

#import <objc/runtime.h>

@implementation UIViewController (BZExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      Class class = [self class];
                      SEL originalSelector = nil;
                      SEL swizzledSelector = nil;
                      Method originalMethod = nil;
                      Method swizzledMethod = nil;
                      for (int i = 0; i < 1; i++)
                      {
                          if (i == 0)
                          {
                              originalSelector = @selector(loadView);
                              swizzledSelector = @selector(swizzledBZ_loadView);
                              originalMethod = class_getInstanceMethod(class, originalSelector);
                              swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
                          }
                          
                          BZAssert((BOOL)(originalMethod && swizzledMethod && originalSelector && swizzledSelector));
                          
                          BOOL didAddMethod =  class_addMethod(class,
                                                               originalSelector,
                                                               method_getImplementation(swizzledMethod),
                                                               method_getTypeEncoding(swizzledMethod));
                          if (didAddMethod)
                          {
                              class_replaceMethod(class,
                                                  swizzledSelector,
                                                  method_getImplementation(originalMethod),
                                                  method_getTypeEncoding(originalMethod));
                          }
                          else
                          {
                              method_exchangeImplementations(originalMethod, swizzledMethod);
                          }
                      }
                  });
}

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

- (void)swizzledBZ_loadView
{
     [self swizzledBZ_loadView];
    CGRect theViewFrameRect = self.view.frame;
    self.view.theMinX = theViewFrameRect.origin.x;
    self.view.theMinY = theViewFrameRect.origin.y;
    UIInterfaceOrientation theDeviceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    if (theDeviceOrientation == UIInterfaceOrientationLandscapeLeft || theDeviceOrientation == UIInterfaceOrientationLandscapeRight)
    {
//        self.view.theWidth = theViewFrameRect.size.height;
//        self.view.theHeight = theViewFrameRect.size.width;
//        self.view.theLandscapeWidth = theViewFrameRect.size.height;
//        self.view.theLandscapeHeight = theViewFrameRect.size.width;
        self.view.theWidth = theViewFrameRect.size.height;
        self.view.theHeight = theViewFrameRect.size.width;
        self.view.theLandscapeWidth = theViewFrameRect.size.height;
        self.view.theLandscapeHeight = theViewFrameRect.size.width;
    }
    else
    {
//        self.view.theWidth = theViewFrameRect.size.height;
//        self.view.theHeight = theViewFrameRect.size.width;
//        self.view.theLandscapeWidth = theViewFrameRect.size.height;
//        self.view.theLandscapeHeight = theViewFrameRect.size.width;
        self.view.theWidth = theViewFrameRect.size.height;
        self.view.theHeight = theViewFrameRect.size.width;
        self.view.theLandscapeWidth = theViewFrameRect.size.width;
        self.view.theLandscapeHeight = theViewFrameRect.size.height;
    }
}

#pragma mark - Getters (Private)

#pragma mark - Lifecycle

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























