//
//  main.m
//  RotationApp
//
//  Created by Boris on 4/13/16.
//  Copyright © 2016 Boris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
