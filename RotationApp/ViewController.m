//
//  ViewController.m
//  RotationApp
//
//  Created by Boris on 4/13/16.
//  Copyright © 2016 Boris. All rights reserved.
//

#import "ViewController.h"

#import "BZExtensionsManager.h"
#import "BZView.h"
#import "SecondViewController.h"

@interface ViewController ()

@property (nonatomic, strong, nonnull) UIView *theBZView;
@property (nonatomic, strong, nonnull) UIView *theGreenView;

@end

@implementation ViewController

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Lifecycle

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.isFirstLoad)
    {
        [self createAllViews];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    
}

#pragma mark - Create Views & Variables

- (void)createAllViews
{
    if (!self.isFirstLoad)
    {
        return;
    }
    self.isFirstLoad = NO;
    self.navigationController.navigationBar.hidden = YES;
    
    UIView *theRedView = [UIView new];
    self.theBZView = theRedView;
    [self.view addSubview:theRedView];
    theRedView.theWidth = theRedView.superview.theWidth - 10;
    theRedView.theMinX = 5;
    theRedView.theMinY = 100;
    theRedView.theHeight = 50;
        
//    theRedView.theLandscapeMinX = 0;
    theRedView.theLandscapeWidth = theRedView.superview.theLandscapeWidth-10;
    theRedView.backgroundColor = [UIColor redColor];
    [theRedView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                             action:@selector(handleGestureTapped:)]];
    
    UIView *theGreenView = [UIView new];
    [self.view addSubview:theGreenView];
    self.theGreenView = theGreenView;
//    NSLog(@"%f", theRedView.theMinX);
//    NSLog(@"%f", theRedView.theMaxY);
//    NSLog(@"%f", theRedView.theWidth);
//    NSLog(@"%f", theRedView.theHeight);
    theGreenView.theWidth = theRedView.theWidth;
    theGreenView.theHeight = theRedView.theHeight;
    theGreenView.theMinX = theRedView.theMinX;
    theGreenView.theMinY = theRedView.theMaxY + 10;
    theGreenView.backgroundColor = [UIColor greenColor];
    
    theGreenView.theLandscapeMinX = theRedView.theLandscapeMinX;
    theGreenView.theLandscapeHeight = 2;
    theGreenView.theLandscapeHeight = 20;
//    theGreenView.theLandscapeHeight = 2;
    theGreenView.frame = CGRectMake(0, 0, 70, 70);
    theGreenView.theMinX = 20;
}

#pragma mark - Actions

#pragma mark - Gestures

- (void)handleGestureTapped:(UITapGestureRecognizer *)theRecognizer
{
    [self.navigationController pushViewController:[SecondViewController new]
                                         animated:YES];
}

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods


//- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
//{
//    [coordinator targetTransform];
//    UIInterfaceOrientation theDeviceOrientation = [UIApplication sharedApplication].statusBarOrientation;
//    if (theDeviceOrientation == UIInterfaceOrientationLandscapeLeft || theDeviceOrientation == UIInterfaceOrientationLandscapeRight)
//    {
//        self.theBZView.theMinX = 50;
//    }
//    else
//    {
//        self.theBZView.theMinX = 300;
//    }
//}

@end






























