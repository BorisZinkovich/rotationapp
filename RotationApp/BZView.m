//
//  BZView.m
//  RotationApp
//
//  Created by Boris on 4/13/16.
//  Copyright © 2016 Boris. All rights reserved.
//

#import "BZView.h"

#import "BZExtensionsManager.h"

@implementation BZView

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (void)dealloc
{
    NSLog(@"BZWork");
}

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Lifecycle

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

//- (void)methodSetObserver
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(receiveOrientationChanged:)
//                                                 name:UIApplicationDidChangeStatusBarOrientationNotification
//                                               object:nil];
//}
//
//#pragma mark - Methods (Private)
//
//- (void)receiveOrientationChanged:(NSNotification *)theNotification
//{
//    UIInterfaceOrientation theDeviceOrientation = [UIApplication sharedApplication].statusBarOrientation;
//    if (theDeviceOrientation == UIInterfaceOrientationLandscapeLeft || theDeviceOrientation ==UIInterfaceOrientationLandscapeRight)
//    {
//        self.theMinX = 300;
//        NSLog(@"Landscape");
//    }
//    else
//    {
//        self.theMinX = 50;
//        NSLog(@"Portrait");
//    }
//}

#pragma mark - Standard Methods

@end






























