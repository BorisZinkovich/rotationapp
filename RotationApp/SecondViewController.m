//
//  SecondViewController.m
//  RotationApp
//
//  Created by Boris on 4/14/16.
//  Copyright © 2016 Boris. All rights reserved.
//

#import "SecondViewController.h"

#import "BZExtensionsManager.h"

@interface SecondViewController ()

@property (nonatomic, strong, nonnull) UIView *theYellowView;

@end

@implementation SecondViewController

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Lifecycle

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.isFirstLoad)
    {
        [self createAllViews];
    }
    UIView *theYellowView = [UIView new];
    self.theYellowView = theYellowView;
    [self.view addSubview:theYellowView];
    theYellowView.theMinX = 5;
    theYellowView.theMinY = 100;
    theYellowView.theHeight = 50;
    theYellowView.theWidth = 50;
    
    theYellowView.theLandscapeMinX = theYellowView.superview.theLandscapeWidth - theYellowView.theLandscapeHeight - 5;
    theYellowView.backgroundColor = [UIColor yellowColor];
    
    [theYellowView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureTapped:)]];

}

- (void)viewDidDisappear:(BOOL)animated
{
    
}

#pragma mark - Create Views & Variables

- (void)createAllViews
{
    if (!self.isFirstLoad)
    {
        return;
    }
    self.isFirstLoad = NO;
    self.navigationController.navigationBar.hidden = YES;
}

#pragma mark - Actions

#pragma mark - Gestures

- (void)handleGestureTapped:(UITapGestureRecognizer *)theRecognizer
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods


@end






























